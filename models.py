import sqlalchemy as db

from app import Base
from base.models import BaseModel


class User(BaseModel, Base):
    __tablename__ = 'users'
    token = db.Column(db.String(22), nullable=True)
    login = db.Column(db.String(128), nullable=False)


class Url(BaseModel, Base):
    __tablename__ = 'urls'
    # Макисмальная длина url в IE
    path = db.Column(db.String(length=2083), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    title = db.Column(db.String(length=1024), nullable=True)


class Event(BaseModel, Base):
    __tablename__ = 'events'
    status_code = db.Column(db.Integer, nullable=True)
    url_id = db.Column(db.Integer, db.ForeignKey('urls.id'), nullable=False)
    response_time = db.Column(db.Time, nullable=True)
    response_size = db.Column(db.Integer, nullable=True)
    active = db.Column(db.Boolean, nullable=True, default=True)
