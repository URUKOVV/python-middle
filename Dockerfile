FROM python:3.8.5
COPY . /opt/python_middle
WORKDIR /app
RUN pip3 install --upgrade pip -r requirements.txt
EXPOSE 5000