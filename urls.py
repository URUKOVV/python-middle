import json

from app import app, session
from flask import request, Response
from permissions import required_token
from models import Url, User, Event
from serializers import UrlSerializer, EventSerializer


@app.route('/urls', methods=['GET'])
@required_token
def get_urls(*args, **kwargs):
    user: User = kwargs.get('user')
    url_serializer = UrlSerializer()
    return url_serializer.dumps(obj=Url.query.filter(Url.user_id == user.id).all(), many=True)


@app.route('/urls', methods=['POST'])
@required_token
def post_url(*args, **kwargs):
    user: User = kwargs.get('user')
    url_serializer = UrlSerializer()
    errors = url_serializer.validate(data=request.form)
    if errors:
        return Response(status=400, response=json.dumps(errors))
    url = Url(user_id=user.id, path=request.form.get('path'), title=request.form.get('title'))
    url.create()
    return Response(status=201, response=url_serializer.dumps(obj=url))


@app.route('/urls/<url_id>', methods=['DELETE'])
@required_token
def delete_url(*args, url_id: int, **kwargs):
    Event.query.filter(Event.url_id == url_id).update({Event.active: False})
    Url.query.filter(Url.id == url_id).delete()
    session.commit()
    return Response(status=200)


@app.route('/events/<url_id>', methods=['GET'])
@required_token
def get_events(*args, url_id: int, **kwargs):
    skip = request.args.get('skip', 0)
    limit = request.args.get('limit', 10)
    url_events = Event.query.filter(Event.url_id == url_id, Event.active == True)
    events = url_events.offset(skip).limit(limit)
    count = url_events.count()
    event_serializer = EventSerializer
    return Response(
        status=200,
        response={
            'items': event_serializer.dumps(obj=events, many=True),
            'items_total': count
        }
    )


@app.route('/statistic', methods=['GET'])
@required_token
def get_statistic(*args, **kwargs):
    status_code = request.args.get('status_code')
    response_time_start, response_time_end = tuple(
        map(lambda time: float(time), str(request.args.get('response_time')).split(','))
    )
    sort = request.args.get('sort') or 'created'
    user = kwargs.get('user')

    # urls = session.query(Event).join(Url, Url.id == Event.url_id).filter(Url.user_id == user.id)
    # filtered_urls = urls.filter(
    #     and_(
    #         Event.status_code == status_code,
    #         Event.response_time >= response_time_start,
    #         Event.response_time <= response_time_end,
    #         Event.active == True
    #     )
    # )
    # count_subquery = session.query(func.count()).filter(Event.url_id == 't1.url_id').subquery()

    # не стал возиться с построением подзапроса средствами sqlalchemy
    events = list(session.execute(
        'SELECT t1.url_id as url_id, '
        'AVG(t1.response_time) as avg_response_time, '
        'COUNT(*)::float / (SELECT COUNT(*) FROM events as t3 WHERE t3.url_id = t1.url_id) as success_rate, '
        'MAX(t1.response_size) as response_size, '
        'MAX(t1.created) as created '
        'FROM events as t1 JOIN urls as t2 ON t2.id = t1.url_id '
        f'WHERE t1.active = true '
        f'AND t1.response_time >= {response_time_start} '
        f'AND t1.response_time <= {response_time_end} '
        f'AND t1.status_code = {status_code} '
        f'AND t2.user_id = {user.id}'
        'GROUP BY t1.url_id '
        f'ORDER BY {sort}'
    ))

    return json.dumps(list(map(
        lambda stat: {
            'url': stat[0], 'avg_response_time': stat[1], 'success_rate': stat[2], 'max_response_size': stat[3]
        },
        events
    )))
