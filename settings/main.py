from os import environ
from celery.schedules import timedelta

env = environ

DATABASE_USER = env.get('DATABASE_USER')
DATABASE_PASSWORD = env.get('DATABASE_PASSWORD')
DATABASE_HOST = env.get('DATABASE_HOST')
DATABASE_NAME = env.get('DATABASE_NAME')

RABBITMQ_HOST = env.get('RABBITMQ_HOST', '192.168.1.103')
RABBITMQ_VHOST = env.get('RABBITMQ_VHOST', '/')
RABBITMQ_PORT = env.get('RABBITMQ_PORT', '5672')
RABBITMQ_DEFAULT_USER = env.get('RABBITMQ_DEFAULT_USER', 'guest')
RABBITMQ_DEFAULT_PASS = env.get('RABBITMQ_DEFAULT_PASS', '')

CELERY_BROKER_URL = f'amqp://{RABBITMQ_DEFAULT_USER}:{RABBITMQ_DEFAULT_PASS}@{RABBITMQ_HOST}/{RABBITMQ_VHOST}'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_ENABLE_UTC = True
CELERY_ALWAYS_EAGER = False
CELERY_WORKER_POOL_RESTARTS = True
CELERY_WORKER_MAX_TASKS_PER_CHILD = 50
CELERY_BEAT_SCHEDULE = {
    'handle_urls': {
        'task': 'tasks.handle_urls',
        'schedule': timedelta(minutes=5)
    }
}
