import datetime

import sqlalchemy as db

from app import session


class BaseModel(object):
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    query = session.query_property()

    def create(self):
        try:
            session.add(self)
            session.commit()
        except Exception:
            session.rollback()
            raise RuntimeError
