import requests
from celery import shared_task

from app import session


@shared_task
def handle_urls():
    from models import Url, Event
    for url in session.execute(Url.query):
        url_object: Url = url._data[0]
        path = url_object.path
        url_id = url_object.id

        try:
            response = requests.get(path)
        except requests.exceptions.ConnectionError:
            continue

        elapsed_time = response.elapsed
        status_code = response.status_code
        event = Event(
            url_id=url_id,
            status_code=status_code,
            response_time=elapsed_time,
            response_size=len(response.content),
        )
        event.create()
