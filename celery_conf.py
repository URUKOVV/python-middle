from celery import Celery
from app import app
import celery
# noinspection pyunresolve
from tasks import *


celery_app = Celery('python_middle', broker=app.config['CELERY_BROKER_URL'])
celery_app.conf.update(app.config)
TaskBase = celery.Task


class ContextTask(TaskBase):
    abstract = True

    def __call__(self, *args, **kwargs):
        with app.app_context():
            return TaskBase.__call__(self, *args, **kwargs)


celery_app.Task = ContextTask
celery_app.app = app

__all__ = ('celery_app',)
