import json
from functools import wraps

from flask import request, Response


def required_token(func):
    from models import User

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            token = request.headers['token']
            user = User.query.filter(User.token == token).first()
            assert user
            kwargs['user'] = user
            return func(*args, **kwargs)
        except KeyError:
            return Response(status=403, response=json.dumps({'Error': 'Access denied, token required.'}))
        except AssertionError:
            return Response(status=403, response=json.dumps({'Error': 'Access denied, invalid token provided.'}))
    return wrapper
