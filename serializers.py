from marshmallow import Schema, fields
from marshmallow.validate import URL


class UrlSerializer(Schema):
    id = fields.Integer(strict=True)
    path = fields.String(required=True, validate=URL())
    title = fields.String(required=False)

    class Meta:
        ordered = True


class EventSerializer(Schema):
    url = fields.URL(required=True)
    created = fields.DateTime(required=False)
    response_time = fields.Float(required=False)
    status_code = fields.Integer(required=False)

    class Meta:
        ordered = True
