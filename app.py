from flask import Flask
from flask_admin import Admin
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, scoped_session, sessionmaker
import settings

app = Flask(__name__)
app.config['DATABASE_USER'] = settings.DATABASE_USER
app.config['DATABASE_PASSWORD'] = settings.DATABASE_PASSWORD
app.config['DATABASE_HOST'] = settings.DATABASE_HOST
app.config['DATABASE_NAME'] = settings.DATABASE_NAME
app.config['CELERY_BROKER_URL'] = settings.CELERY_BROKER_URL
app.config['CELERY_ACCEPT_CONTENT'] = settings.CELERY_ACCEPT_CONTENT
app.config['CELERY_TASK_SERIALIZER'] = settings.CELERY_TASK_SERIALIZER
app.config['CELERY_ENABLE_UTC'] = settings.CELERY_ENABLE_UTC
app.config['CELERY_ALWAYS_EAGER'] = settings.CELERY_ALWAYS_EAGER
app.config['CELERY_WORKER_POOL_RESTARTS'] = settings.CELERY_WORKER_POOL_RESTARTS
app.config['CELERY_WORKER_MAX_TASKS_PER_CHILD'] = settings.CELERY_WORKER_MAX_TASKS_PER_CHILD
app.config['CELERYBEAT_SCHEDULE'] = settings.CELERY_BEAT_SCHEDULE

engine = create_engine(
    f"postgresql://{app.config.get('DATABASE_USER')}:{app.config.get('DATABASE_PASSWORD')}@{app.config.get('DATABASE_HOST')}/{app.config.get('DATABASE_NAME')}"
)
session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()

# noinspection pyunresolve
from urls import *

if __name__ == '__main__':
    app.run(debug=False)
