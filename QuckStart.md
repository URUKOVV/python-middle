Перед запуском необходимо прописать настройки в settings/local.py:
    **`базы данных`**
    **`celery`**
    **`rabbitmq`**

Воркер celery можно запустить при помощи **`worker.py`**
Запуск beat: **`celery -A celery_conf.celery_app beat -l info`**
Бэкенд сервер: **`python -m flask run --host 0.0.0.0`**

Для запуска на linux есть **`*.sh`** скрипты в папке **`scripts`**

