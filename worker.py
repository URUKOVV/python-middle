"""
A python script which starts celery worker and auto reload it when any code change happens.
"""

import os
import subprocess
import time

import psutil
from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer

celery_working_dir = os.path.dirname(os.path.abspath(__file__))

celery_purge_cmdline = "celery -A celery_conf.celery_app purge -f"
celery_cmdline = "celery -A celery_conf.celery_app worker --pool=solo -E -l info".split(" ")


class MyHandler(PatternMatchingEventHandler):
    def on_any_event(self, event):
        print("detected change. event = {}".format(event))

        for proc in psutil.process_iter():
            proc_cmdline = self._get_proc_cmdline(proc)
            if not proc_cmdline or len(proc_cmdline) < len(celery_cmdline):
                continue

            is_celery_worker = (
                'python' in proc_cmdline[0].lower()
                and celery_cmdline[0].lower() in proc_cmdline[1].lower()
                and celery_cmdline[1] == proc_cmdline[2]
            )

            if not is_celery_worker:
                continue

            proc.kill()
            print("Just killed {} on working dir {}".format(proc_cmdline, proc))

        run_worker()

    def _get_proc_cmdline(self, proc):
        try:
            return proc.cmdline()
        except Exception:
            return []


def purge():
    os.chdir(celery_working_dir)
    subprocess.Popen(celery_purge_cmdline)


def run_worker():
    print("Ready to call {} ".format(celery_cmdline))
    os.chdir(celery_working_dir)
    subprocess.Popen(celery_cmdline)
    print("Done callling {} ".format(celery_cmdline))


if __name__ == "__main__":
    purge()
    run_worker()

    event_handler = MyHandler(patterns=["*tasks.py"])
    observer = Observer()
    observer.schedule(event_handler, celery_working_dir, recursive=True)
    observer.start()
    print("file change observer started")

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
